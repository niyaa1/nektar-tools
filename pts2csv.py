#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.


import argparse
import logging
import os

import numpy as np


import plotPtsFile

args = None
logger = logging.getLogger('root')


def main():
    global args

    parser = argparse.ArgumentParser(
        description='converts a Nektar++ .pts file to csv')
    parser.add_argument('infile', type=str, help='pts file')
    parser.add_argument('outfile', type=str, nargs='?', help='variable')
    parser.add_argument('--debug', action='store_true', dest='debug')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    if args.infile.endswith('.pts'):
        data = plotPtsFile.read_pts(args.infile)
    else:
        print("ERROR: unknown file extension")
        return False

    if args.outfile:
        outfile = args.outfile
    else:
        (fn, ext) = os.path.splitext(args.infile)
        outfile = fn + ".csv"

    np.savetxt(outfile, data, delimiter=", ", header=", ".join(data.dtype.names))

if __name__ == "__main__":
    main()
