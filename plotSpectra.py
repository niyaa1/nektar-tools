#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import print_function, division

import re
import sys
import os
import logging
import argparse
from copy import deepcopy
from itertools import cycle
from cycler import cycler
from collections import OrderedDict

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import Grid

import h5py

logger = logging.getLogger('root')

def get_styles():

    styles = cycler(dashes=[
        (None, None),
        (8, 1),
        (4, 1),
        (2, 1),
        (4, 1, 1, 1),
        (4, 1, 2, 1),
        (4, 1, 1, 1, 1, 1),
    ])

    colors = mpl.rcParams['axes.prop_cycle']
    #cm = plt.get_cmap('viridis')
    #max_plts = 4
    #colors = cycler(color=[cm(i) for i in np.linspace(0.95, 0.0, max_plts)])

    styles = styles * colors

    return styles


def outside_legend(fig, ax, x0=1, y0=1, direction = "v", padpoints = 3,**kwargs):
    otrans = ax.figure.transFigure
    t = ax.legend(bbox_to_anchor=(x0,y0), loc='upper center', bbox_transform=otrans,**kwargs)
    plt.tight_layout(pad=0)
    ax.figure.canvas.draw()
    plt.tight_layout(pad=0)

    if direction == "v":
        ppar = [0,-padpoints/72.]
    else:
        [-padpoints/72.,0]

    trans2=mpl.transforms.ScaledTranslation(ppar[0],ppar[1], fig.dpi_scale_trans) + ax.figure.transFigure.inverted()
    tbox = t.get_window_extent().transformed(trans2 )
    bbox = ax.get_position()
    if direction=="v":
        ax.set_position([bbox.x0, bbox.y0,bbox.width, tbox.y0-bbox.y0])
    else:
        ax.set_position([bbox.x0, bbox.y0,tbox.x0-bbox.x0, bbox.height])


def time_to_freq(time_data, pad_fac=0, window='boxcar', var_name='p', dt=None, cutoff=None):
    print('performing FFT')

    if not var_name in time_data.dtype.names:
        print('field "{0}" not found'.format(var_name))
        sys.exit(-1)

    signalLength = time_data.shape[-1]

    if pad_fac > 0:
        nFFT = int(2 ** (np.ceil(np.log2(signalLength)))) * pad_fac
    else:
        nFFT = int(2 ** (np.floor(np.log2(signalLength))))

    if window == 'bartlett':
        filt = np.bartlett(signalLength)
    elif window == 'blackman':
        filt = np.blackman(signalLength)
    elif window == 'hamming':
        filt = np.hamming(signalLength)
    elif window == 'hanning':
        filt = np.hanning(signalLength)
    elif window == 'boxcar':
        filt = np.kaiser(signalLength, 0)
    else:
        print('unknown window function')
        sys.exit(-1)

    # prepare array
    spectrum = np.zeros(nFFT // 2, dtype=[('f', 'float32'), ('amp', 'float32'), ('phase', 'float32')])

    #hp_win = hp[var_name]
    hp_win = (time_data[var_name] - np.mean(time_data[var_name])) * filt * signalLength / np.sum(filt)
    hp_FFT = np.fft.fft(hp_win, n=nFFT)

    # get amplitudes
    amp = 2.0 * np.abs(hp_FFT) / signalLength
    # split spectrum
    spectrum['amp'] = amp[0:nFFT // 2]

    phase = np.angle(hp_FFT)
    spectrum['phase'] = phase[0:nFFT // 2]

    if not dt:
        dt = time_data['time'][2] - time_data['time'][1]
    f = np.fft.fftfreq(nFFT, d=dt)
    spectrum['f'] = f[0:nFFT // 2]

    print(
        u'delta_t={:8.2E}s f_sample={:8.2E}Hz f_nyquist={:8.2E}Hz t_total={:8.2E}s N_bins={} f_num={:8.2E}'.format(
            dt,
            1 / dt,
            1 / (2 * dt),
            dt * signalLength,
            nFFT,
            1 / (dt * signalLength)
        ))

    if cutoff:
        idx = np.searchsorted(spectrum['f'], cutoff)
        spectrum = spectrum[1:idx]

    return spectrum

def plot_spectra_twocol(spectra_groups, args):

    plt.style.use(args.style)

    fs = plt.rcParams['figure.figsize']
    if "beamer" in args.style:
        fs = [fs[0], fs[1]]
    else:
        fs = [fs[0], fs[0]/4]
    if (args.multirow):
        fs[1] = 0.8 * args.multirow * fs[1]
    fig = plt.figure(figsize=fs)
    gs0 = mpl.gridspec.GridSpec(2, 1, height_ratios=[1, 1E23], wspace=0.0, hspace=0.0)

    # add a top axes, hide frame
    ax0 = fig.add_subplot(gs0[0], frameon=False)
    ax0.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    axes = []

    if (args.multirow):
        pass
        gs = mpl.gridspec.GridSpec(args.multirow, 2, wspace=0.1, hspace=0.1)

        for r in range(0, args.multirow):
            plot_groups = []
            for g in spectra_groups:
                if g['row'] == r :
                    plot_groups.append(g)

            axes.append(fig.add_subplot(gs[2*r]))
            subplot_spectra(fig, axes[-1], plot_groups, args)
            axes.append(fig.add_subplot(gs[2*r+1]))
            subplot_spectra(fig, axes[-1], plot_groups, args)

    elif (args.phase):
        pass
        #gs = mpl.gridspec.GridSpec(2, 1)

        #axes.append(fig.add_subplot(gs[0]))
        #axes.append(fig.add_subplot(gs[1]))

        #subplot_spectra(fig, axes[0], spectra_groups, args)
        #subplot_phase(fig, axes[1], spectra_groups, args)

    else:
        gs = mpl.gridspec.GridSpecFromSubplotSpec(1, 2,
            subplot_spec=gs0[1], wspace=0.1)
        axes.append(fig.add_subplot(gs[0]))
        axes.append(fig.add_subplot(gs[1]))
        subplot_spectra(fig, axes[0], spectra_groups, args)
        subplot_spectra(fig, axes[1], spectra_groups, args)

    handles = []
    labels = []

    for ii, ax in enumerate(axes):

        h, l = ax.get_legend_handles_labels()
        handles += h
        labels += l

        if ax in axes[0::2]:
            # first col
            ax.set(ylabel='{}'.format(args.ytitle))
            (xmin, xmax) = axes[-1].get_xlim()
            ax.set(xlim=(xmin, xmax/4))
        else:
            [label.set_visible(False) for label in ax.get_yticklabels()]
            ax.get_shared_y_axes().join(axes[ii-1], ax)

        if ax in axes[0:2]:
            # first row
            pass
        else:
            pass

        if ax in axes[-2:]:
            # last row
            ax.set( xlabel=r'$f$ [$\mathrm{Hz}$]')
        else:
            [label.set_visible(False) for label in ax.get_xticklabels()]



        if args.xlog:
                ax.set(xscale='log')
        if args.ylog:
            ax.set(yscale='log')
            ax.yaxis.set_major_locator(mpl.ticker.LogLocator(numticks=100))
            ax.yaxis.set_minor_locator(mpl.ticker.LogLocator(subs=np.arange(0.1, 1, 0.1), numticks=100))
            ax.yaxis.set_minor_formatter(mpl.ticker.NullFormatter())

        if args.title:
            ax.set(title=args.title)

        ax.grid(True)

    tmp = dict(zip(labels, handles))
    if args.legcol > 0:
        ax0.legend(tmp.values(), tmp.keys(), bbox_to_anchor=(0,1.02,1,0.2), loc="lower left",
                 borderaxespad=0, ncol=args.legcol, frameon=False)

    if args.tight:
        fig.tight_layout()

    if args.out:
        for o in args.out:
            print('writing {0}'.format(o))
            if args.tight:
                plt.savefig(o)
            else:
                plt.savefig(o, bbox_inches='tight')

    if args.show:
        plt.show()


def plot_spectra(spectra_groups, args):

    plt.style.use(args.style)

    fs = plt.rcParams['figure.figsize']
    if "beamer" in args.style:
        fs = [fs[0], fs[1]]
    else:
        fs = [fs[0], fs[0]/4]

    if (args.multirow):
        fs = [fs[0]/2 * 1.2, fs[0]/3.2 * args.multirow]

    fig = plt.figure(figsize=fs)
    gs0 = mpl.gridspec.GridSpec(2, 1, height_ratios=[1, 1E23], wspace=0.0, hspace=0.0)

    # add a top axes, hide frame
    ax0 = fig.add_subplot(gs0[0], frameon=False)
    ax0.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    axes = []

    if (args.multirow):
        gs = mpl.gridspec.GridSpecFromSubplotSpec(args.multirow, 1, subplot_spec=gs0[1])

        for r in range(0, args.multirow):
            plot_groups = []
            for g in spectra_groups:
                if g['row'] == r :
                    plot_groups.append(g)

            axes.append(fig.add_subplot(gs[r]))
            subplot_spectra(fig, axes[-1], plot_groups, args)

    elif (args.phase):
        gs = mpl.gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=gs0[1], hspace=0.2)

        axes.append(fig.add_subplot(gs[0]))
        axes.append(fig.add_subplot(gs[1]))

        subplot_spectra(fig, axes[0], spectra_groups, args)
        subplot_phase(fig, axes[1], spectra_groups, args)

    else:
        axes.append(fig.add_subplot(gs0[1]))
        subplot_spectra(fig, axes[0], spectra_groups, args)

    handles = []
    labels = []

    for ii, ax in enumerate(axes):

        h, l = ax.get_legend_handles_labels()
        handles += h
        labels += l

        if ax == axes[0]:
            if args.xlog:
                ax.set(xscale='log')
            if args.ylog:
                ax.set(yscale='log')
                ax.yaxis.set_major_locator(mpl.ticker.LogLocator(numticks=100))
                ax.yaxis.set_minor_locator(mpl.ticker.LogLocator(subs=np.arange(0.1, 1, 0.1), numticks=100))
                ax.yaxis.set_minor_formatter(mpl.ticker.NullFormatter())
        else:
            ax.get_shared_x_axes().join(axes[0], ax)
            (ymin, ymax) = axes[0].get_ylim()
            (xmin, xmax) = axes[0].get_xlim()
            ax.set(ylim=(ymin, ymax), xlim=(xmin, xmax))

        if ax == axes[-1]:
            ax.set( xlabel=r'$f$ [$\mathrm{Hz}$]')
        else:
            [label.set_visible(False) for label in ax.get_xticklabels()]

        ax.set(ylabel='{}'.format(args.ytitle))

        if args.title:
            ax.set(title=args.title)

        ax.grid(True)

    tmp = OrderedDict(zip(labels, handles))
    if args.legcol > 0:
        ax0.legend(tmp.values(), tmp.keys(), bbox_to_anchor=(0,1.02,1,0.2), loc="lower left",
                 borderaxespad=0, ncol=args.legcol, frameon=False)

    if args.tight:
        fig.tight_layout()

    if args.out:
        for o in args.out:
            print('writing {0}'.format(o))
            if args.tight:
                plt.savefig(o)
            else:
                plt.savefig(o, bbox_inches='tight')

    if args.show:
        plt.show()


def subplot_spectra(fig, ax, spectra_groups, args):

    for group in spectra_groups:

        spectra = group['data']

        color = list(get_styles())[group['color']]['color']

        # plot all point amplitudes
        for n_mic, sty in zip(spectra, cycle(get_styles())):

            # convert to dB
            amp = spectra[n_mic]['amp']
            if args.decibel:
                amp = 20.0 * np.log10(amp / 2.0e-5)
            elif args.psd:
                amp = 20.0 * np.log10(amp / 2.0e-5) / spectra[n_mic]['f']

            if args.single:
                ax.plot(
                    spectra[n_mic]['f'], amp,
                    label="{} (mic{})".format(group['label'], n_mic),
                    color=sty['color'],
                    dashes=sty['dashes'],
                    )
            elif len(spectra) != 1:
                ax.plot(
                    spectra[n_mic]['f'], amp,
                    color=color,
                    linewidth=0.1 * mpl.rcParams['lines.linewidth'],
                )

        # plot envelopes of point amplitudes
        max_amp = next(iter(spectra.values()))['amp']
        for n_mic in spectra:
            max_amp = np.fmax(spectra[n_mic]['amp'], max_amp)
        if args.decibel:
            max_amp = 20.0 * np.log10(max_amp / 2.0e-5)

        # print info
        #print("freq-range {}: ({}, {}) [Hz]".format(idx, min(spectra[0]['f']), max(spectra['f'])))
        print(
            "amp-range  {}: ({}, {}) [dB]".format(group['label'], min(max_amp), max(max_amp)))
        if not args.single:
            ax.plot(
                next(iter(spectra.values()))['f'], max_amp,
                color=color,
                label=group['label'],
                linewidth=0.5*mpl.rcParams['lines.linewidth'],
            )

    (ymin, ymax) = ax.get_ylim()
    (xmin, xmax) = ax.get_xlim()
    xmin = max(xmin, 0)
    if args.xmin != None:
        xmin = args.xmin
    if args.xmax != None:
        xmax = args.xmax
    if args.ymin != None:
        ymin = args.ymin
    if args.ymax != None:
        ymax = args.ymax


    ax.set(ylim=(ymin, ymax),
                xlim=(xmin, xmax),
            )

    # plot analytic eigenfrequencies
    if args.mfreq:
        mfreq = np.unique(args.mfreq)
        mfreq.sort()

        af_max = (ymax - ymin) * 0.95 + ymin
        af_min = (ymax - ymin) * 0.06 + ymin
        af_minl = (ymax - ymin) * 0.05 + ymin

        for ii, freq in enumerate(mfreq):
            ax.plot(
                [freq, freq], [af_min, af_max],
                color='grey',
                linewidth=2 * mpl.rcParams['lines.linewidth'],
                alpha=0.7,
                label='$f_{{{}}} = {}$ Hz'.format(ii, freq),
            )
            ax.annotate('$f_{{{}}}$'.format(ii),
                        xy=(freq, af_minl),
                        xycoords='data',
                        horizontalalignment='center',
                        verticalalignment='top',
                        color='grey',)


def subplot_phase(fig, ax, spectra_groups, args):

    for group in spectra_groups:

        spectra = group['data']

        color = list(get_styles())[group['color']]['color']

        for n_mic, sty in zip(spectra, cycle(get_styles())):

            if args.single:
                ax.plot(
                    spectra[n_mic]['f'], spectra[n_mic]['phase'],
                    label="{} (mic{})".format(group['label'], n_mic),
                    color=sty['color'],
                    dashes=sty['dashes'],
                    )
            else:
                ax.plot(
                    spectra[n_mic]['f'], spectra[n_mic]['phase'],
                    color=color,
                    label=group['label'],
                    #linewidth=0.1 * mpl.rcParams['lines.linewidth'],
                )

def main():
    parser = argparse.ArgumentParser(
        description='plots the spectra from a HDF5 spectra file over frequency')
    parser.add_argument('--debug', action='store_true')

    parser.add_argument('-g', '--group', type=str, nargs='+', action='append',
                        required=True, help='[ROW] NAME COLOR_NUM FILE [MIC_NUM ...]  row (in --multirow mode), name, color number, spectra file and mic numbers of a spectra group')
    parser.add_argument('-m', '--mfreq', type=float, nargs='*', help='mark frequency')
    parser.add_argument('--decibel', action='store_true',  help="convert to decibel")
    parser.add_argument('--phase', action='store_true',  help="plot phase, too")
    parser.add_argument('--psd', action='store_true',  help="convert to decibel/Hz")
    parser.add_argument('--single', action='store_true',  help="plot single spectrum of a group")
    parser.add_argument('--ylog', action='store_true',  help="log10 y axis")
    parser.add_argument('--xlog', action='store_true',  help="log10 x axis")
    parser.add_argument('--out', '-o', type=str, action='append')
    parser.add_argument('--show', '-s', action='store_true')
    parser.add_argument('--style', action='store', default='default', help="plot style (default: %(default)s)")
    parser.add_argument('--twocol', action='store_true', help="plot into two columns")
    parser.add_argument('--multirow', type=int, help="plot into N rows")
    parser.add_argument('--tight', action='store_true',  help="tight layout")
    parser.add_argument('--title', type=str)
    parser.add_argument('--ytitle', type=str, default='p', help="y var name (default: %(default)s)")
    parser.add_argument('--xmin', dest='xmin', type=int)
    parser.add_argument('--xmax', dest='xmax', type=int)
    parser.add_argument('--ymin', dest='ymin', type=float)
    parser.add_argument('--ymax', dest='ymax', type=float)
    parser.add_argument('--legcol', type=int, default=3, help='legend columns (default: %(default)s)')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module) -12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    if not args.show and not args.out:
        args.show = True
    spectra_groups = []

    for group in args.group:

        if args.multirow and len(group) < 4:
            print("group too short")
            sys.exit(-1)
        elif (not args.multirow) and len(group) < 3:
            print("group too short")
            sys.exit(-1)

        if args.multirow:
            row = int(group.pop(0))
        else:
            row = 0
        name = group.pop(0)
        color = int(group.pop(0))

        spectra_file = h5py.File(group.pop(0), "r")
        spectra_group = spectra_file['spectra']

        print("loaded {} spectra".format(len(spectra_group)))

        try:
            group = [int(n) for n in group]
        except:
            print("mic num conversion failed for {}".format(group))
            sys.exit(-1)

        freq_data = {}
        for n_mic in spectra_group:
            if len(group) > 0:
                if not int(n_mic) in group:
                    continue

            freq_data[int(n_mic)] = spectra_group[n_mic]

        if len(freq_data) == 0:
            print('no matching spectra found')
            sys.exit(-1)

        spectra_groups.append(
            {'label': name,
             'data': freq_data,
             'color': color,
             'row': row
            })

    if args.out:
        args.out = [os.path.expandvars(s) for s in args.out]
        args.out = [os.path.expanduser(s) for s in args.out]

    if args.twocol:
        plot_spectra_twocol(spectra_groups, args)
    else:
        plot_spectra(spectra_groups, args)


    spectra_file.close()





if __name__ == "__main__":
    main()
