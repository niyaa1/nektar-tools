nektar-tools
========

A bunch of python scripts for working with nektar++. See requirements.txt for a list of dependencies or use pipenv for setting up a virtual environment.
