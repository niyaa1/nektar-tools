#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.




import re
import sys
import os
import logging
import argparse

import numpy as np
import h5py
import natsort

from plotHistoryPoints import join_sections, open_mic_files
from plotSpectra import time_to_freq

logger = logging.getLogger('root')


def main():
    parser = argparse.ArgumentParser(
        description='Converts external spectra data to our own format')
    parser.add_argument('--debug', action='store_true', dest='debug')

    parser.add_argument('in_file', nargs='+', type=str, help='infile name')
    parser.add_argument('out_file', type=str, help='outfile name')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    spectra_file = h5py.File(args.out_file, "w")
    spectra_group = spectra_file.create_group("spectra")

    max_len = 0
    freq_data = []
    for ii, fn in enumerate(args.in_file):

        fd = np.genfromtxt(
            fname=fn,
            names=('f', 'amp'),
            delimiter=",",
            dtype=(np.float32, np.float32),
            skip_header=1,
        )
        fd = np.sort(fd, order='f')
        fd = fd[fd['f']>0]

        max_len = max(fd.shape[0], max_len)

        #fd['amp'] = np.sqrt(fd['amp'] * fd['f'] * (2**(1/6) - 2**(-1/6)))

        # convert from decibel to Pascal
        fd['amp'] = 2E-5 * 10**(fd['amp'] / 20)

        # convert from power density to power spectrum
        #fd['amp'] = np.sqrt(fd['amp'])

        freq_data.append(fd)

    print(max_len)


    for ii, fd in enumerate(freq_data):

        fd = np.pad(fd, (0, max_len - fd.shape[0]), mode='constant', constant_values=np.nan)
        spectra_group.create_dataset('{}'.format(ii), data=fd)

    spectra_file.close()


if __name__ == "__main__":
    main()
