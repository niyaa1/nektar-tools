#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.


import re
import sys
import os
import logging
import argparse

import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt
import matplotlib as mpl
import h5py

from multiprocessing import Pool, TimeoutError

import natsort

CHUNKSIZE=1000

logger = logging.getLogger('root')


def main():
    parser = argparse.ArgumentParser(
        description='do a FFT at every point in the domain and visualize the resulting modes')
    parser.add_argument('--debug', action='store_true')

    subparsers = parser.add_subparsers(
        dest="subparser_name", title='subcommands', help='additional help')

    parser_readall = subparsers.add_parser('readall', help='read all snapshots at once')
    parser_readall.add_argument('in_file', type=str, nargs='+', help='infile name')
    parser_readall.add_argument('out_file', type=str, help='outfile name')
    parser_readall.add_argument('--var', default='p', type=str,
                             help="variable to process (default: %(default)s)")


    parser_3dfft = subparsers.add_parser('3dfft', help='perform 3dfft on snapshots')
    parser_3dfft.add_argument('in_file', type=str, help='infile name')
    parser_3dfft.add_argument('dt', type=float, help='time step')
    parser_3dfft.add_argument('out_file', type=str, help='outfile name')
    parser_3dfft.add_argument('--var', default='p', type=str,
                             help="variable to process (default: %(default)s)")
    parser_3dfft.add_argument('--padfac', action='store', type=int, default=1, help="Padding factor (default: %(default)s)")
    parser_3dfft.add_argument('--window', action='store', type=str, default='hamming',
                        help='Window function to apply. Either "hanning", "hamming", "bartlett", "blackman" or "boxcar" (default: %(default)s)')
    parser_3dfft.add_argument('--cutoff', action='store', type=float, help="Cutoff frequency (default: no cutoff)")
    parser_3dfft.add_argument('--steps', type=float, nargs=2, help='use steps from n1 to n1 (default: %(default)s)')

    parser_series = subparsers.add_parser('series', help='convert spectra to csv files')
    parser_series.add_argument('in_file', type=str, help='infile name')
    parser_series.add_argument('out_file', type=str, help='outfile name')
    parser_series.add_argument('--freq', type=str, nargs='*', help='limit to given frequencies')
    parser_series.add_argument('--nsteps', type=int, default=8, help='number of steps (default: %(default)s)')

    parser_maxima = subparsers.add_parser('maxima', help='compute pressure at maximum phase')
    parser_maxima.add_argument('in_file', type=str, help='infile name')
    parser_maxima.add_argument('out_file', type=str, help='outfile name')


    parser_linf = subparsers.add_parser('linf', help='plot Linf(amp)')
    parser_linf.add_argument('in_file', type=str, help='infile name')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )


    if args.subparser_name == "readall":
        read_all_snapshots(args)
    elif args.subparser_name == "3dfft":
        do_3dfft(args)
    elif args.subparser_name == "maxima":
        maxima(args)
    elif args.subparser_name == "series":
        gen_series(args)
    elif args.subparser_name == "linf":
        plot_linf(args)
    else:
        print("wrong command")
        sys.exit(-1)


def read_all_snapshots(args):

    out_file = h5py.File(args.out_file, "w")

    names = None
    V = None
    for ii, fn in enumerate(natsort.natsorted(args.in_file)):
        print("reading {} .. ".format(fn), end='')
        sys.stdout.flush()
        if ii == 0:
            with open(fn, 'r') as f:
                first_line = f.readline()
                names = first_line[1:-1].split(',')
            names = [n.strip() for n in names]

            tmp = np.loadtxt(
                fname=fn,
                usecols=(0,1,2,names.index(args.var)),
                #dtype={'names': names, 'formats': [np.float32,]*len(names)},
                delimiter=',',
                unpack=True
            )

            xyz = np.zeros(shape=(3, tmp.shape[1]))
            xyz[0] = tmp[0]
            xyz[1] = tmp[1]
            xyz[2] = tmp[2]

            out_file.create_dataset("xyz", data=xyz.T)
            V = out_file.create_dataset("snapshots", (len(args.in_file), tmp.shape[1]), dtype=np.float32)

            print('writing')
            V[ii] = tmp[3]

        else:
            tmp = np.loadtxt(
                fname=fn,
                usecols=(names.index(args.var)),
                #dtype={'names': names, 'formats': [np.float32,]*len(names)},
                delimiter=',',
                unpack=True
            )
            print('writing')
            V[ii] = tmp

    out_file.close()


def time_to_freq(time_data, dt, nFFT, window='hamming'):

    signalLength = time_data.shape[-1]

    if window == 'bartlett':
        filt = np.bartlett(signalLength)
    elif window == 'blackman':
        filt = np.blackman(signalLength)
    elif window == 'hamming':
        filt = np.hamming(signalLength)
    elif window == 'hanning':
        filt = np.hanning(signalLength)
    elif window == 'boxcar':
        filt = np.kaiser(signalLength, 0)
    else:
        print('unknown window function')
        sys.exit(-1)

    hp_win = (time_data - np.mean(time_data)) * filt * signalLength / np.sum(filt)
    hp_FFT = np.fft.rfft(hp_win, n=nFFT)

    amp = 2.0 * np.abs(hp_FFT) / signalLength
    amp = amp[0:nFFT // 2]
    phase = np.angle(hp_FFT)
    phase = phase[0:nFFT // 2]
    freq = np.fft.fftfreq(nFFT, d=dt)
    freq = freq[0:nFFT // 2]

    return (freq, amp, phase)


def time_to_freq_b(V, dt, nFFT, window='hamming'):

    npoints = V.shape[1]
    nfreqs = int(nFFT/2)

    freqs = np.zeros(shape=(npoints, nfreqs))
    amps = np.zeros(shape=(npoints, nfreqs))
    phases = np.zeros(shape=(npoints, nfreqs))

    for ii, v in enumerate(V.T):
        freqs[ii], amps[ii], phases[ii] = time_to_freq(v, dt, nFFT, window='hamming')

    return (freqs, amps, phases)


def xdmf_header(args, xdmf_file, npoints=0, name=None):

    print('<?xml version="1.0" ?>',file=xdmf_file)
    print('<Xdmf Version="2.0">',file=xdmf_file)
    print('  <Domain>',file=xdmf_file)
    print('    <Grid Name="spectra">',file=xdmf_file)
    print('      <Topology NodesPerElement="1" TopologyType="Polyvertex"/>',file=xdmf_file)
    print('      <Geometry GeometryType="XYZ">',file=xdmf_file)
    print('        <DataItem Dimensions="{} 3" Format="HDF">'.format(npoints),file=xdmf_file)
    print('           {}.h5:/xyz'.format(name),file=xdmf_file)
    print('        </DataItem>',file=xdmf_file)
    print('      </Geometry>',file=xdmf_file)

def xdmf_footer(args, xdmf_file):
    print('    </Grid>',file=xdmf_file)
    print('  </Domain>',file=xdmf_file)
    print('</Xdmf>',file=xdmf_file)

def xdmf_attrib(args, xdmf_file, npoints=0, name=None, f_string=None, prefix=None):

    if prefix:
        print('      <Attribute Center="Node" AttributeType="Scalar"  Name="{0}_{1}">'.format(prefix,f_string),file=xdmf_file)
        print('        <DataItem Dimensions="{} 1" Format="HDF">'.format(npoints),file=xdmf_file)
        print('           {2}.h5:/{0}/{1}'.format(prefix, f_string,name),file=xdmf_file)
        print('        </DataItem>',file=xdmf_file)
        print('      </Attribute>',file=xdmf_file)
    else:
        print('      <Attribute Center="Node" AttributeType="Scalar"  Name="{1}">'.format(prefix,f_string),file=xdmf_file)
        print('        <DataItem Dimensions="{} 1" Format="HDF">'.format(npoints),file=xdmf_file)
        print('           {2}.h5:/{1}'.format(prefix, f_string,name),file=xdmf_file)
        print('        </DataItem>',file=xdmf_file)
        print('      </Attribute>',file=xdmf_file)

def do_3dfft(args):

    in_file = h5py.File(args.in_file, "r")
    V = in_file['snapshots']
    xyz = in_file['xyz']

    if (V.shape[1] != xyz.shape[0]):
        print('xyz and snapshots shapes do not match. V: {} xyz: {}'.format(V.shape, xyz.shape))
        sys.exit(-0)

    npoints = V.shape[1]

    signalLength = V.shape[0]
    if args.padfac > 0:
        nFFT = int(2 ** (np.ceil(np.log2(signalLength)))) * args.padfac
    else:
        nFFT = int(2 ** (np.floor(np.log2(signalLength))))

    nfreqs = int(nFFT/2)

    freqs = np.zeros(shape=(npoints, nfreqs))
    amps = np.zeros(shape=(npoints, nfreqs))
    phases = np.zeros(shape=(npoints, nfreqs))

    results = [None,]*int(npoints/CHUNKSIZE)
    with Pool() as pool:
        for ii in range(int(npoints/CHUNKSIZE)):
            print("init: {0:03.0f} %".format(ii/int(npoints/CHUNKSIZE) * 100))
            results[ii] = pool.apply_async(time_to_freq_b, (V[:,ii*CHUNKSIZE:(ii+1)*CHUNKSIZE], args.dt, nFFT))

        for ii, res in enumerate(results):
            print("collect: {0:03.0f} %".format(ii/int(npoints/CHUNKSIZE) * 100))
            (freqs[ii*CHUNKSIZE:(ii+1)*CHUNKSIZE], amps[ii*CHUNKSIZE:(ii+1)*CHUNKSIZE], phases[ii*CHUNKSIZE:(ii+1)*CHUNKSIZE]) = res.get(timeout=10)

    name, ext = os.path.splitext(args.out_file)
    print("writing to {}".format(name + ".xdmf"))
    xdmf_file = open(name + ".xdmf", 'w')

    xdmf_header(args, xdmf_file, npoints=npoints, name=name)

    print("writing to {}".format(name + ".h5"))
    h5_file = h5py.File(name + ".h5", "w")
    g_amp = h5_file.create_group("amp")
    g_phase = h5_file.create_group("phase")
    for ii in range(nfreqs):
        print("write : {0:03.0f} %".format(ii/nfreqs * 100))

        f_string = "{0:07.2f}".format(freqs[0,ii])

        g_amp.create_dataset("{0}".format(f_string), data=amps[:,ii])
        g_phase.create_dataset("{0}".format(f_string), data=phases[:,ii])

        xdmf_attrib(args, xdmf_file, npoints=npoints, name=name, f_string=f_string, prefix='amp')
        xdmf_attrib(args, xdmf_file, npoints=npoints, name=name, f_string=f_string, prefix='phase')

    linf = np.zeros(shape=(nfreqs,))
    for ii, f in enumerate(h5_file['amp/']):
        amp = h5_file['amp/{}'.format(f)]
        linf[ii] = np.amax(amp)
    h5_file.create_dataset("linf_amps", data=linf)

    h5_file.create_dataset("xyz", data=in_file['xyz'])
    h5_file.close()

    in_file.close()

    xdmf_footer(args, xdmf_file)

    xdmf_file.close()


def gen_series(args):

    in_file = h5py.File(args.in_file, "r")
    npoints = in_file['amp/0000.00'].shape[0]

    for ii, frac in enumerate(np.linspace(0,2,args.nsteps)):

        frac = frac * np.ones(shape=in_file['amp/0000.00'].shape)

        out_name, ext = os.path.splitext(args.out_file)
        out_name = out_name + "_{0:02.0f}".format(ii)

        out_file = h5py.File(out_name + ".h5", "w")
        out_file.create_dataset("xyz", data=in_file['xyz'])
        xdmf_file = open(out_name + ".xdmf", 'w')
        xdmf_header(args, xdmf_file, npoints=npoints, name=out_name)

        for f in in_file['amp/']:
            if args.freq and not f in args.freq:
                continue

            print("{}, {}".format(ii, f))
            amp = in_file['amp/{}'.format(f)]
            phase = in_file['phase/{}'.format(f)]

            val = amp * np.cos(np.pi * frac + phase)
            out_file.create_dataset(f, data = val)
            xdmf_attrib(args, xdmf_file, npoints=npoints, name=out_name, f_string=f)

        xdmf_footer(args, xdmf_file)
        xdmf_file.close()
        out_file.close()

    in_file.close()



def plot_linf(args):

    in_file = h5py.File(args.in_file, "r+")
    npoints = in_file['amp/0000.00'].shape[0]
    nfreqs = len(in_file['amp/'])

    freqs = np.zeros(shape=(nfreqs,))
    linf = in_file['linf_amps']

    for ii, f in enumerate(in_file['amp/']):
        freqs[ii] = float(f)

    fs = plt.rcParams['figure.figsize']

    fig = plt.figure(figsize=fs)
    ax = fig.add_subplot(111)

    ax.plot(freqs, linf)

    fig.tight_layout()

    plt.show()


    in_file.close()

def maxima(args):

    in_file = h5py.File(args.in_file, "r")
    npoints = in_file['amp/0000.00'].shape[0]

    out_name, ext = os.path.splitext(args.out_file)
    out_file = h5py.File(out_name + ".h5", "w")
    out_file.create_dataset("xyz", data=in_file['xyz'])
    xdmf_file = open(out_name + ".xdmf", 'w')
    xdmf_header(args, xdmf_file, npoints=npoints, name=out_name)

    for f in in_file['amp/']:
        print("{}".format(f))
        amp = in_file['amp/{}'.format(f)]
        phase = in_file['phase/{}'.format(f)]

        max_pt = np.argmax(amp)

        fracs = np.linspace(0,2,200)
        tmp = np.cos(np.pi * fracs  + phase[max_pt])
        max_frac_id = np.argmax(tmp)

        val = amp * np.cos(np.pi * fracs[max_frac_id] + phase)

        out_file.create_dataset(f, data = val)
        xdmf_attrib(args, xdmf_file, npoints=npoints, name=out_name, f_string=f)

    xdmf_footer(args, xdmf_file)
    xdmf_file.close()
    out_file.close()
    in_file.close()




if __name__ == "__main__":
    main()
