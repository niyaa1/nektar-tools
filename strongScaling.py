#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.

# example bash script for case generation:
#    for i in 24 48 72 96 120; do
#        cp -ar template_hpb "hpb_${i}"
#        cd "hpb_${i}"
#        sed -i "s:_NRANK_:$i:g" start_lcluster.sh
#        sbatch start_lcluster.sh
#        cd ..
#    done


import os
import sys
import re
import argparse
import logging

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import optimize

ERR_QUANTITY="median"


def main():

    parser = argparse.ArgumentParser(
        description='creates strong scaling plots')
    parser.add_argument('--debug', action='store_true', dest='debug')

    subparsers = parser.add_subparsers(
        dest="subparser_name", title='subcommands', help='additional help')

    parser_parse = subparsers.add_parser('parse', help='parse logs')

    parser_plot = subparsers.add_parser('plot', help='plot CPU times')
    parser_plot.add_argument('stats_file', type=str, nargs='+', help='log files')
    parser_plot.add_argument('--out', '-o', type=str)
    parser_plot.add_argument('--show', '-s', action='store_true')
    parser_plot.add_argument('--speedup', action='store_true')
    parser_plot.add_argument(
        '--style', default='default', help="plot style (default: %(default)s)")
    parser_plot.add_argument('--xmin', type=float)
    parser_plot.add_argument('--xmax', type=float)
    parser_plot.add_argument('--ymin', type=float)
    parser_plot.add_argument('--ymax', type=float)

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    if args.subparser_name == "parse":
        parse_logs()
    elif args.subparser_name == "plot":
        if not args.show and not args.out:
            args.show = True

        plot_stats(args)
    else:
        print('error')

def parse_logs():

    res = {}

    dirs = os.listdir(os.getcwd())
    for d in dirs:
        if not os.path.isdir(d):
            continue
        if not '_' in d:
            continue
        if 'template' in d:
            continue

        section, nranks = d.split('_')

        logfile = os.path.join(os.getcwd(), d, "log.000")

        if not os.path.isfile(logfile):
            continue

        times = []
        with open(logfile) as lf:
            for line in lf:
                match = re.search(r'.*CPU Time: ([^s]+)s', line)
                if match:
                    times.append(float(match.group(1)))

        fact = 1
        if section == "stage2":
            fact = 12
        if section == "stage3":
            fact = 16

        if not section in res:
            res[section] = []
        res[section].append((int(nranks) * fact, np.median(times), np.mean(times), np.std(times)))

    for section in res:
        res[section] = np.array(res[section], dtype=[('nranks', np.int), ('median', np.float), ('mean', np.float), ('std', np.float)])
        np.savetxt(section  + ".csv",
                   res[section],
                   header=", ".join(res[section].dtype.names),
                   delimiter=', ')


def plot_stats(args):



    plt.style.use(args.style)

    fs = mpl.rcParams['figure.figsize']
    fs[1] = fs[0] * 1 / 2

    fig = plt.figure(figsize=(fs[0], fs[1]))
    ax = fig.add_subplot(111)

    for filename in args.stats_file:
        data = np.genfromtxt(filename, delimiter=",", names=True)
        data = np.sort(data, order="nranks")
        data[ERR_QUANTITY] = data[ERR_QUANTITY] / 10
        min_ranks = np.min(data['nranks'])

        speedup = data[ERR_QUANTITY][data['nranks'] == min_ranks] / data[ERR_QUANTITY]

        title = filename.split(".")[0]

        if not args.speedup:
            p = ax.plot(data['nranks'], data[ERR_QUANTITY],
                    marker='o',
                    label=title)
            k0 = [0.3,]
            # Target function a*x^b
            fitfunc = lambda k, x: k[0] * x ** -1
            # Distance to the target function
            errfunc = lambda k, x, y: (np.log(fitfunc(k, x)) - np.log(y))
            # regress
            k1, success = optimize.leastsq(errfunc, k0[:], args=(data['nranks'], data[ERR_QUANTITY]))
            ax.plot(data['nranks'], fitfunc(k1, data['nranks']),
                    color=p[0].get_color(),
                    dashes=(1,1))
        else:
            p = ax.plot(data['nranks'], speedup,
                    marker='o',
                    label=title)

            fitfunc = lambda x: x / min_ranks
            ax.plot(data['nranks'], fitfunc(data['nranks']),
                    color=p[0].get_color(),
                    dashes=(1,1))

    (ymin, ymax) = ax.get_ylim()
    (xmin, xmax) = ax.get_xlim()
    xmin = max(xmin, 0)
    if args.xmin != None:
        xmin = args.xmin
    if args.xmax != None:
        xmax = args.xmax
    if args.ymin != None:
        ymin = args.ymin
    if args.ymax != None:
        ymax = args.ymax

    ax.set(
        xlabel=r'MPI Ranks [-]',
        ylabel=r'CPU time [s]',
        ylim=(ymin, ymax),
        xlim=(xmin, xmax),
    )

    if not args.speedup:
        ax.set(yscale='log', xscale='log')

    ax.grid(True)

    ax.legend(loc='best')

    plt.tight_layout()

    if args.out:
        print('writing {0}'.format(args.out))
        plt.savefig(args.out, dpi=300)

    if args.show:
        plt.show()


if __name__ == "__main__":
    main()


