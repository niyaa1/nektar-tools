#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function, division

import re
import sys
import os
import logging
import argparse
import tempfile
import shutil

import numpy as np
from numpy.lib.recfunctions import append_fields
import pandas as pd
import h5py

logger = logging.getLogger('root')

def parse_header(hist_file):
    # get number of points and column names
    header_data = {}
    header_data['points'] = []
    with open(hist_file) as fn_in:
        while True:
            line = fn_in.readline()

            if line == '' or line[0] != '#':
                break

            match = re.search(r'\(:(.*),\)', line)
            if match:
                matches = match.group(1)
                names = ['time']
                names.extend(matches.split(','))
                header_data['fields'] = names
                continue

            match = re.search(
                r'^#\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)\s+([^\s]+)', line)
            if match:
                header_data['points'].append(
                    (match.group(2), match.group(3), match.group(4)))
                continue

    logger.info(
        'Found {} points in file {}'.format(len(header_data['points']), hist_file))

    return header_data


def his2csv(hist_file, header_data, csv_dir, downsample=1):

    prefix = "{0}/mic".format(csv_dir)

    csv_files = []

    n_points = len(header_data['points'])

    fn_tmp = []
    for (i, pt) in enumerate(header_data['points']):
        csv_files.append('{0}_{1:03}.csv'.format(prefix, i + 1))
        fn = open(csv_files[-1], 'w')
        print("# Point at ({},{},{})".format(pt[0], pt[1], pt[2]), file=fn)
        print(' '.join(header_data['fields']), file=fn)
        fn_tmp.append(fn)

    logger.info(
        'Splitting hist file {}. This will take a while'.format(hist_file))

    with open(hist_file) as fn_in:

        cur_point = 0
        cur_iter = 0
        while True:
            line = fn_in.readline()

            if line == '':
                break
            if line[0] == '#':
                cur_point = 0
                continue

            if cur_iter % downsample == 0:
                print(line, file=fn_tmp[cur_point], end='')

            cur_point += 1
            if cur_point > n_points - 1:
                cur_point = 0
                cur_iter += 1

    for fn in fn_tmp:
        fn.close()

    return csv_files


def fix_dt(data, new_dt):

    # we assume that the first value is correct
    first_t = data['time'][0]

    new_t = np.arange(len(data['time'])) * new_dt + first_t

    data = append_fields(data, 'time_broken', data['time'])
    data['time'] = new_t

    return data


def csv2hdf5(csv_files, out_file, dt=None):

    logger.info("parsing tmp files")

    hdf5_file = h5py.File(out_file, "w")
    h5_mics = hdf5_file.create_group("mics")

    for ii, mf in enumerate(csv_files):

        logger.info('{0:3.0f} %'.format(100 * ii / len(csv_files)))

        with open(mf) as fn_in:
            line = fn_in.readline()
            match = re.search(r'\((.*),(.*),(.*)\)', line)
            loc = [float(k) for k in match.groups()]

        t = pd.read_csv(
            mf,
            delimiter='\s+',
            header=1,)
        t = t.to_records(index=False)

        if dt:
            t = fix_dt(t, dt)

        mic_dset = h5_mics.create_dataset('{}'.format(ii), data=t)
        mic_dset.attrs['location'] = loc

    hdf5_file.close()


def main():
    parser = argparse.ArgumentParser(
        description='Converts a Nektar++ HistoryPoints file to HDF5 format')
    parser.add_argument('hist_file',  type=str, help='history files')
    parser.add_argument('--tmp-dir', type=str, action='store')
    parser.add_argument(
        '--csv', action='store_true', help="write csv files instead of hdf5")
    parser.add_argument(
        '--force', action='store_true', help="overwrite existing files")
    parser.add_argument('--dt', type=float, help="delta t (including --downsample)")
    parser.add_argument('--downsample', type=int, default=1, help="use only every Nth sample")
    parser.add_argument('--debug', action='store_true', dest='debug')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    out_file, ext = os.path.splitext(args.hist_file)
    out_file = out_file + ".h5"
    if os.path.exists(out_file) and not args.force:
        logger.error("output file {} exists".format(out_file))
        sys.exit(-1)

    header_data = parse_header(args.hist_file)

    del_tmpdir = False
    if args.csv:
        if args.tmp_dir:
            tmpdir = args.tmp_dir
        else:
            tmpdir, ext = os.path.splitext(args.hist_file)
            tmpdir += "_csv"
        if not os.path.exists(tmpdir):
            os.makedirs(tmpdir)
        del_tmpdir = False
    else:
        tmpdir = tempfile.mkdtemp()
        del_tmpdir = True

    if not os.path.isdir(tmpdir):
        logger.error("{} already exists but its not a dir".format(tmpdir))
        sys.exit(-1)

    tmp_files = his2csv(args.hist_file, header_data, tmpdir, args.downsample)

    if not args.csv:
        csv2hdf5(tmp_files, out_file, args.dt)

    if del_tmpdir:
        logger.info("deleting {}".format(tmpdir))
        shutil.rmtree(tmpdir)


if __name__ == "__main__":
    main()
