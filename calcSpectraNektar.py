#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function, division

import re
import sys
import os
import logging
import argparse

import numpy as np
import h5py
import natsort

from plotHistoryPoints import join_sections, open_mic_files
from plotSpectra import time_to_freq

logger = logging.getLogger('root')


def main():
    parser = argparse.ArgumentParser(
        description='Compute the spectrum from a HDF5 history file')
    parser.add_argument('--debug', action='store_true', dest='debug')

    parser.add_argument('in_file', nargs='+', type=str, help='infile name')
    parser.add_argument('out_file', type=str, help='outfile name')
    parser.add_argument('--var', default='p', type=str)
    parser.add_argument('--mic-num', type=int, nargs='*', help='mic numbers to plot')
    parser.add_argument('--padfac', action='store', type=int, default=1, help="Padding factor (default: %(default)s)")
    parser.add_argument('--window', action='store', type=str, default='hamming',
                        help='Window function to apply. Either "hanning", "hamming", "bartlett", "blackman" or "boxcar" (default: %(default)s)')
    parser.add_argument('--cutoff', action='store', type=float, help="Cutoff frequency (default: no cutoff)")
    parser.add_argument('--steps', type=float, nargs=2, help='use steps from n1 to n1 (default: %(default)s)')


    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    if args.steps:
        try:
            steps = [int(k) for k in args.steps]
        except:
            print("--steps does not containvalid integers")
            sys.exit(-1)
    else:
        steps = [0,-1]

    mic_groups, mic_files = open_mic_files(natsort.natsorted(args.in_file))

    print("loaded {} mics from {} files".format(len(mic_groups[0]), len(mic_groups)))

    spectra_file = h5py.File(args.out_file, "w")
    spectra_group = spectra_file.create_group("spectra")

    for n_mic in mic_groups[0]:
        if args.mic_num:
            if not int(n_mic) in args.mic_num:
                continue

        joint_mic = join_sections(mic_groups, int(n_mic))

        if "location" in mic_groups[0][n_mic].attrs:
            loc = mic_groups[0][n_mic].attrs['location']
            print("processing mic #{} at x = ({}, {}, {})".format(n_mic, loc[0], loc[1], loc[2]), end='')
        print(", using steps {} to {} (out of {} steps)".format(steps[0], steps[1], joint_mic.shape[0]))

        joint_mic = joint_mic[steps[0]:steps[1]]

        f = time_to_freq(joint_mic, pad_fac=args.padfac, window=args.window, cutoff=args.cutoff, var_name=args.var)
        spectra_group.create_dataset('{}'.format(n_mic), data=f)

    for fh in mic_files:
        fh.close()
    spectra_file.close()

if __name__ == "__main__":
    main()
