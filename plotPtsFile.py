#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.



import shutil
import argparse
import logging
import sys
import os
import re

import numpy as np
import matplotlib as mpl
from scipy import interpolate
import matplotlib.pyplot as plt

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.mplot3d import Axes3D

args = None
logger = logging.getLogger('root')

def read_pts(filename):

    if os.path.isdir(filename):
        data = []

        pts_files = os.listdir(filename)
        pts_files.sort()
        for pts_file in pts_files:
            if pts_file.endswith(".pts"):
                data.append(read_pts_file(os.path.join(filename, pts_file)))

        return np.concatenate(data)

    else:
        return read_pts_file(filename)


def read_pts_file(filename):

    names = None
    dim = 0
    tag_count = 0
    with open(filename, 'r') as fn:
        cnt = 1
        while True:
            line = fn.readline()
            if line == '' or cnt > 100:
                break
            match = re.search(r'<POINTS.*FIELDS="(\S+)".*>', line)
            if match:
                names = match.group(1)
            match = re.search(r'<POINTS.*DIM="(\S+)".*>', line)
            if match:
                dim = int(match.group(1))
            match = re.search(r'<.*>', line)
            if match:
                tag_count += 1

            cnt += 1

    if not names:
        print("FIELDS not found in the first 10 lines, giving up")
        return False
    names = names.split(",")

    x = ["x", "y", "z,"]
    names = x[0:dim] + names

    return np.genfromtxt(filename,
                         skip_header=tag_count + 1,
                         skip_footer=tag_count,
                         delimiter=" ",
                         dtype={
                             'names': names,
                             'formats': [np.float] * len(names)
                         },
                         unpack=True)


def calc_dim(data):

    print("   min({}) = {}".format(args.var, np.min(data[args.var])))
    print("   max({}) = {}".format(args.var, np.max(data[args.var])))
    print("  mean({}) = {}".format(args.var, np.mean(data[args.var])))
    print("median({}) = {}".format(args.var, np.median(data[args.var])))

    dim = 0
    if 'x' in data.dtype.names:
        dim += 1
        print("min(x), max(x) = ({}, {})".format(
            np.min(data['x']), np.max(data['x'])))
    if 'y' in data.dtype.names:
        dim += 1
        print("min(y), max(y) = ({}, {})".format(
            np.min(data['y']), np.max(data['y'])))
    if 'z' in data.dtype.names:
        dim += 1
        print("min(z), max(z) = ({}, {})".format(
            np.min(data['z']), np.max(data['z'])))

    ids = np.where(data[args.var] >= args.threshold)
    ids = np.array(ids[0])
    if args.xmin and dim >= 1:
        ids = np.intersect1d(ids, np.where(data['x'] > args.xmin))
    if args.xmax and dim >= 1:
        ids = np.intersect1d(ids, np.where(data['x'] < args.xmax))
    if args.ymin and dim >= 2:
        ids = np.intersect1d(ids, np.where(data['y'] > args.ymin))
    if args.ymax and dim >= 2:
        ids = np.intersect1d(ids, np.where(data['y'] < args.ymax))
    if args.zmin and dim >= 3:
        ids = np.intersect1d(ids, np.where(data['z'] > args.xmin))
    if args.zmax and dim >= 3:
        ids = np.intersect1d(ids, np.where(data['z'] < args.zmax))

    if dim == 3 and np.amin(data['z'][ids]) == np.amax(data['z'][ids]):
        dim -= 1
        print("reducing dimension")
    if dim == 2 and np.amin(data['y'][ids]) == np.amax(data['y'][ids]):
        dim -= 1
        print("reducing dimension")
    if dim == 1 and np.amin(data['x'][ids]) == np.amax(data['x'][ids]):
        dim -= 1
        print("reducing dimension")

    if dim == 1:
        sortids = np.argsort(data['x'][ids])
        ids = ids[sortids]

    return (dim, ids)


def plot_scatter(data, dim, ids):

    print("plotting {} points".format(ids.shape[0]))
    d = data[args.var][ids]
    s = np.abs(d) / np.max(np.abs(d))

    fig = plt.figure()
    fig.canvas.set_window_title("{} - {}".format(args.file, args.var))
    if dim == 3:
        ax = fig.add_subplot(111, projection='3d', aspect='equal')
    elif dim == 2:
        ax = fig.add_subplot(111, aspect='equal')
    else:
        ax = fig.add_subplot(111)

    if dim == 3:
        #for i in ids:
            #print("{0:10.8e} {1:10.8e} {2:10.8e} {3:10.8e}".format(data['x'][i], data['y'][i], data['z'][i], data[args.var][i]))
        sct = ax.scatter(data['x'][ids], data['y'][ids], data['z'][
            ids], cmap=plt.cm.YlGnBu, c=d, s=1 + 200 * s**3)
    elif dim == 2:
        #for i in ids:
            #print("{0:10.8e} {1:10.8e} {2:10.8e}".format(data['x'][i], data['y'][i], data[args.var][i]))
        sct = ax.scatter(
            data['x'][ids], data['y'][ids], cmap=plt.cm.YlGnBu, c=d, s=1 + 200 * s**3)
    elif dim == 1:
        #for i in ids:
            #print("{0:10.8e} {1:10.8e}".format(data['x'][i], data[args.var][i]))
        ax.plot(data['x'][ids], data[args.var][ids], marker='o')
    else:
        #for i in ids:
            #print("{0:10.8e}".format(data[args.var][i]))
        ax.plot(data[args.var][ids], marker='o')

    # divide axes and create a new, colorbar axes cbax
    if dim >= 2:
        divider = make_axes_locatable(ax)
        cbax = divider.append_axes("right", size="5%", pad=0.05)
        cbar = fig.colorbar(sct, cax=cbax, label=args.var)
        cbar.ax.artists.remove(cbar.outline)

    ax.set_xlabel('X')
    if dim >= 2:
        ax.set_ylabel('Y')
    if dim == 3:
        ax.set_zlabel('Z')

    plt.tight_layout()

    plt.show()


def plot_interp2d(data, dim, ids):

    xlim = (np.nanmin(data['x'][ids]), np.nanmax(data['x'][ids]))
    ylim = (np.nanmin(data['y'][ids]), np.nanmax(data['y'][ids]))
    vlim = (np.nanmin(data[args.var][ids]), np.nanmax(data[args.var][ids]))

    grid_x, grid_y = np.meshgrid(
        np.linspace(xlim[0], xlim[1], args.xres, endpoint=True),
        np.linspace(ylim[0], ylim[1], args.yres, endpoint=True),
    )
    # this gives us row organized grid arrays, so we need to transpose them in
    # order to obtain the same column shape as np.mgrid would have returned
    grid = (np.transpose(grid_x), np.transpose(grid_y))
    points = np.transpose(np.array([data['x'][ids], data['y'][ids]]))

    interp_d = interpolate.griddata(
        points=points,
        values=data[args.var][ids],
        xi=grid,
        method='linear'
    )

    fig = plt.figure()
    fig.canvas.set_window_title("{} - {}".format(args.file, args.var))
    ax = fig.add_subplot(111, aspect='equal')

    V1 = np.linspace(vlim[0], vlim[1], args.vres, endpoint=True)
    contf = ax.contourf(grid[0], grid[1], interp_d, V1)
    contf.set_cmap(plt.cm.jet)

    # divide axes and create a new, colorbar axes cbax
    divider = make_axes_locatable(ax)
    cbax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(contf, cax=cbax, label=args.var)
    cbar.ax.artists.remove(cbar.outline)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')

    plt.tight_layout()

    plt.show()

def main():
    global args

    parser = argparse.ArgumentParser(
        description='plots a Nektar++ .pts file')
    parser.add_argument('file', type=str, help='pts file')
    parser.add_argument('var', type=str, help='variable')
    parser.add_argument('--interpolate', action='store_true', help="interpolate data to a regulat grid")
    parser.add_argument('--xres', type=int, help="interpolation resolution in x dir (default: %(default)s)", default=400)
    parser.add_argument('--yres', type=int, help="interpolation resolution in y dir (default: %(default)s)", default=400)
    parser.add_argument('--vres', type=int, help="interpolation resolution of the values (default: %(default)s)", default=256)
    parser.add_argument(
        '--threshold', type=float, help='threshold (default: %(default)s)', default=-1E23)
    parser.add_argument('--xmin', type=float, help="xmin")
    parser.add_argument('--xmax', type=float, help="xmax")
    parser.add_argument('--ymin', type=float, help="ymin")
    parser.add_argument('--ymax', type=float, help="ymax")
    parser.add_argument('--zmin', type=float, help="zmin")
    parser.add_argument('--zmax', type=float, help="zmax")
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    if args.file.endswith('.pts'):
        data = read_pts(args.file)

    elif args.file.endswith('.csv'):
        data = np.genfromtxt(args.file,
                             delimiter=",",
                             names=True,
                             unpack=True)
    else:
        print("ERROR: unknown file extension")
        return False

    (dim, ids) = calc_dim(data)

    if args.interpolate:
        if dim != 2:
            print('interpolate only implemented for 2D')
            sys.exit(-1)

        plot_interp2d(data, dim, ids)
    else:
        plot_scatter(data, dim, ids)

if __name__ == "__main__":
    main()
