#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.


import shutil
import argparse
import logging
import sys
import os

import numpy as np
import matplotlib as mpl
from scipy.interpolate import griddata
import matplotlib.pyplot as plt

import plotPtsFile

logger = logging.getLogger('root')


def main():
    global args

    parser = argparse.ArgumentParser(
        description='plots the differences betweem two Nektar++ .pts files')
    parser.add_argument('file1', type=str, help='pts file')
    parser.add_argument('file2', type=str, help='pts file')
    parser.add_argument('var', type=str, help='variable')
    parser.add_argument(
        '--threshold', type=float, help='threshold', default=0.0)
    parser.add_argument('--debug', action='store_true', dest='debug')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    data = plotPtsFile.read_pts(args.file1)

    print("1 min({}) = {}".format(args.var, np.min(data[args.var])))
    print("1 max({}) = {}".format(args.var, np.max(data[args.var])))

    data2 = plotPtsFile.read_pts(args.file2)

    print("2 min({}) = {}".format(args.var, np.min(data2[args.var])))
    print("2 max({}) = {}".format(args.var, np.max(data2[args.var])))

    diff = data2[args.var] - data[args.var]

    print("diff min({}) = {}".format(args.var, np.min(diff)))
    print("diff max({}) = {}".format(args.var, np.max(diff)))
    print("diff mean({}) = {}".format(args.var, np.mean(diff)))
    print("diff median({}) = {}".format(args.var, np.median(diff)))

    dim = 1
    if 'y' in data.dtype.names:
        dim += 1
    if 'z' in data.dtype.names:
        dim += 1

    fig = plt.figure()
    if dim == 3:
        ax = fig.add_subplot(111, projection='3d')
    else:
        ax = fig.add_subplot(111)
    ids = np.where(np.abs(diff) >= args.threshold)
    d = diff[ids]
    r = np.max(np.abs(d))
    s = np.abs(d) / r

    if dim == 3:
        sct = ax.scatter(data['x'][ids], data['y'][ids], data['z'][
                         ids], cmap=plt.cm.RdBu, c=d, s=200 * s**3, vmin=-r, vmax=r)
    elif dim == 2:
        sct = ax.scatter(data['x'][ids], data['y'][
                         ids], cmap=plt.cm.RdBu, c=d, s=200 * s**3, vmin=-r, vmax=r)
    else:
        sct = ax.scatter(
            data['x'][ids], cmap=plt.cm.RdBu, c=d, s=200 * s**3, vmin=-r, vmax=r)

    fig.colorbar(sct, ax=ax)

    ax.set_xlabel('X')
    if dim >= 2:
        ax.set_ylabel('Y')
    if dim == 3:
        ax.set_zlabel('Z')

    plt.show()


if __name__ == "__main__":
    main()
