#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.



import re
import sys
import os
import logging
import argparse

import numpy as np
import h5py
import natsort

from plotHistoryPoints import join_sections, open_mic_files
from plotSpectra import time_to_freq

logger = logging.getLogger('root')


def main():
    parser = argparse.ArgumentParser(
        description='Compute the spectrum from a PRECISE-UNS history file')
    parser.add_argument('--debug', action='store_true', dest='debug')

    parser.add_argument('in_file', nargs='+', type=str, help='infile name')
    parser.add_argument('out_file', type=str, help='outfile name')
    parser.add_argument('var_name', type=str, help='variable name')
    parser.add_argument('dt', type=float, help='dt')
    parser.add_argument('--mic-num', type=int, nargs='*', help='mic numbers')
    parser.add_argument('--padfac', action='store', type=int, default=1, help="Padding factor (default: %(default)s)")
    parser.add_argument('--window', action='store', type=str, default='hamming',
                        help='Window function to apply. Either "hanning", "hamming", "bartlett", "blackman" or "boxcar" (default: %(default)s)')
    parser.add_argument('--cutoff', action='store', type=float, help="Cutoff frequency (default: no cutoff)")
    parser.add_argument('--skip', type=float, default=0, help='Skip first n steps (default: %(default)s)')
    parser.add_argument('--skiprows', type=int, default=0, help='Skip first n rows of mon files (default: %(default)s)')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    spectra_file = h5py.File(args.out_file, "w")
    spectra_group = spectra_file.create_group("spectra")

    for ii, fn in enumerate(args.in_file):

        with open(fn, 'r') as fd:

            while True:
                line = fd.readline()
                if line == '':  # Stops if end of file reached
                    break

                # match title line
                match = re.search(
                    r'.+monitor point: x y z cell =\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)".*', line)
                if match:
                    x = float(match.group(1))
                    y = float(match.group(2))
                    z = float(match.group(3))
                    id = int(match.group(4))

                # match variables line
                match = re.search(r'variables = (\S+)', line)
                if match:
                    fields = match.group(1).split(',')

                    break

        if not args.var_name in fields:
            print('Variable name "{}" not found'.format(args.var_name))
            print('Valid names are: {}'.format(', '.join(fields)))
            sys.exit(-1)

        time_data = np.genfromtxt(
            fname=fn,
            names=fields,
            dtype=None,
            skip_header=3 + args.skiprows
        )

        if args.skip:
            try:
                args.skip = int(args.skip)
                print("skipping steps with i < {}".format(args.skip))
            except:
                print("args.skip is not a valid integer")
                sys.exit(-1)

            skip_idx = np.searchsorted(time_data['step'], args.skip)
            time_data = time_data[skip_idx:]

            #skip_idx = np.searchsorted(time_data['step'], 50E3)
            #time_data = time_data[:skip_idx]

        f = time_to_freq(time_data, pad_fac=args.padfac, window=args.window, cutoff=args.cutoff, var_name=args.var_name, dt=args.dt)
        spectra_group.create_dataset('{}'.format(ii), data=f)

    spectra_file.close()


if __name__ == "__main__":
    main()
